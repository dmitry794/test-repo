pipeline {
    agent any
    parameters {
        string(name: 'ProjectName', defaultValue: 'test project', description: 'Building test project')
    }

    options {
        // Prevent running the same jobs in parallel
        disableConcurrentBuilds()

        // Abort build after 1 hour
        timeout(time: 1, unit: 'HOURS')

        // Remove old builds after 30 days, keep max. 3 builds at the same time
        buildDiscarder(
          logRotator(
            daysToKeepStr:'30',
            numToKeepStr:'3',
            artifactDaysToKeepStr:'30',
            artifactNumToKeepStr:'3'))

        // Print timestamp for every log entry in console output
        timestamps()
    }

    environment {
        CUSTOM_BUILD_TAG = get_custom_build_tag()

        DOCKER_REGISTRY = 'https://trololosh-registry.com'
        DOCKER_IMAGE_NAME = 'ololosh-admin'
        tag = sh(script: 'grep -Po "(?<=^VERSION = ).*" setup.py | cut -d\'"\' -f2', returnStdout: true).trim()
        DOCKER_IMAGE_TAG = "${env.BRANCH_NAME == "develop" ? "${tag}-dev" : "${tag}"}"
    }

    /**
     Remark when using "sh" in a step with 'returnStatus: true' you need to handle an error yourself!
     If you set it to false an error is thrown automatically.
    */
    stages {
        stage('echo environment') {
            steps {
                sh label: 'building', script: """echo ${env.CUSTOM_BUILD_TAG}; echo ${env.DOCKER_IMAGE_TAG}"""
                sh label: 'building', script: """echo ${env.DOCKER_IMAGE_TAG}"""
            }
            post {
                failure {
                    error 'failed to echo environment'
                }
            }
        }
        stage('conditional stage') {
            when {
                anyOf { branch 'develop'; branch 'master' }
            }
            steps {
                script {
			echo "im develop or master"
                    }
                }
            }

        stage('conditional dev stage') {
            when {
                branch 'develop'
            }
            steps {
                script {
			        echo "im develop only"
                    }
                }
            }
        }

    post {
        always {
            sh label: 'cleaning up', script: """echo 'im cleanup'"""
        }
    }
}

def get_custom_build_tag() {
    node('master') {
        return env.GIT_BRANCH.toLowerCase().replaceAll("[^a-z0-9]+","-") + '_' + env.BUILD_NUMBER
    }
}

